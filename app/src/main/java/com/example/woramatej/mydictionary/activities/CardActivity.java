package com.example.woramatej.mydictionary.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.woramatej.mydictionary.R;
import com.example.woramatej.mydictionary.model.Vocab;

public class CardActivity extends AppCompatActivity {

    private TextView word, meaning;

    private Vocab vocab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);
        Intent intent = getIntent();
        vocab = (Vocab)intent.getSerializableExtra("vocab");
        initComponents();
    }

    private void initComponents() {
        word = (TextView)findViewById(R.id.word);
        meaning = (TextView)findViewById(R.id.meaning);
        word.setText(vocab.getWord());
        meaning.setText(vocab.getMeaning());
    }
}
