package com.example.woramatej.mydictionary.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.woramatej.mydictionary.R;
import com.example.woramatej.mydictionary.model.Vocab;
import com.example.woramatej.mydictionary.util.Dictionary;

import org.json.JSONException;

public class EditVocabActivity extends AppCompatActivity {

    private Button saveBtn;

    private EditText word, meaning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_vocab);
        initComponents();
    }

    private void initComponents() {
        word = (EditText)findViewById(R.id.word);
        meaning = (EditText)findViewById(R.id.meaning);

        saveBtn = (Button)findViewById(R.id.save_btn);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
                finish();
            }
        });
    }

    private void save() {
        Vocab vocab = new Vocab( word.getText().toString(), meaning.getText().toString() );
        try {
            Dictionary.getInstance().saveVocab(this, vocab);
        } catch ( JSONException e ) {
            e.printStackTrace();
        }
    }
}
