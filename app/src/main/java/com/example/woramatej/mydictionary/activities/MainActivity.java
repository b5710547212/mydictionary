package com.example.woramatej.mydictionary.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.woramatej.mydictionary.R;
import com.example.woramatej.mydictionary.adapters.VocabsAdapter;
import com.example.woramatej.mydictionary.model.Vocab;
import com.example.woramatej.mydictionary.util.Dictionary;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Vocab> vocabs;

    private Button newBtn;
    private ListView vocabsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadVocabs();
    }

    private void initComponents() {
        vocabsList = (ListView)findViewById(R.id.vocabs_list);
        vocabs = new ArrayList<Vocab>();
        VocabsAdapter vocabsAdapter = new VocabsAdapter( this, R.layout.vocab_list, vocabs );
        vocabsList.setAdapter(vocabsAdapter);
        vocabsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, CardActivity.class );
                intent.putExtra("vocab", vocabs.get(position));
                startActivity(intent);
            }
        });
        newBtn = (Button)findViewById(R.id.add_vocab_btn);
        newBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, EditVocabActivity.class);
                startActivity(intent);
            }
        });
    }

    private void loadVocabs() {
        try {
            for ( Vocab v : Dictionary.getInstance().loadVocabs(this) ) {
                vocabs.add(v);
            }
        } catch (JSONException e ) {
            e.printStackTrace();
        }
    }


}
