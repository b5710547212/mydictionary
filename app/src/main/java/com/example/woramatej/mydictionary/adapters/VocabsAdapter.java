package com.example.woramatej.mydictionary.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.woramatej.mydictionary.R;
import com.example.woramatej.mydictionary.model.Vocab;

import java.util.List;

/**
 * Created by Woramate J on 8/3/2559.
 */
public class VocabsAdapter extends ArrayAdapter<Vocab> {

    public VocabsAdapter(Context context, int resource, List<Vocab> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;

        if ( v == null ) {
            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.vocab_list, null);
        }

        TextView word = (TextView)v.findViewById(R.id.word);
        TextView meaning = (TextView)v.findViewById(R.id.meaning);

        word.setText(getItem(position).getWord());
        meaning.setText(getItem(position).getWord());

        return v;

    }
}
