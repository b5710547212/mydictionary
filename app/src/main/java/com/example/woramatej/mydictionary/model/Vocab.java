package com.example.woramatej.mydictionary.model;

import java.io.Serializable;

/**
 * Created by Woramate J on 8/3/2559.
 */
public class Vocab implements Serializable{
    private String word, meaning;

    public Vocab(String word, String meaning) {
        this.word = word;
        this.meaning = meaning;
    }

    public String getWord() { return word;}

    public void setWord(String word) { this.word = word; }

    public String getMeaning() { return meaning; }

    public void setMeaning(String meaning) { this.meaning = meaning; }
}
