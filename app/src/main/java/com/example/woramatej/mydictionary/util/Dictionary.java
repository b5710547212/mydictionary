package com.example.woramatej.mydictionary.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.woramatej.mydictionary.model.Vocab;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Woramate J on 8/3/2559.
 */
public class Dictionary {

    private static Dictionary instance;
    private String DB = "VOCABS";
    private SharedPreferences.Editor editor;

    private Dictionary() {}

    public static Dictionary getInstance() {
        if ( instance == null ) instance = new Dictionary();
        return instance;
    }

    public void saveVocab( Context context, Vocab vocab )throws JSONException {
        editor = context.getSharedPreferences( DB, Context.MODE_PRIVATE ).edit();
        List<Vocab> vocabs = loadVocabs( context );
        vocabs.add(vocab);
        saveVocabJson( new Gson().toJson(vocabs) );
    }

    public void saveVocabJson( String vocabJson ) {
        editor.putString( DB, vocabJson );
        editor.commit();
    }

    public List<Vocab> loadVocabs( Context context ) throws JSONException {
        String vocabsJson = context.getSharedPreferences( DB, Context.MODE_PRIVATE ).getString( DB, null );
        if ( vocabsJson == null || vocabsJson.trim().equals("") ) return new ArrayList<Vocab>();
        Type type = new TypeToken< List<Vocab> >() {}.getType();
        return new Gson().fromJson(vocabsJson, type );
    }

}
